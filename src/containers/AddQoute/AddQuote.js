import React, {Component} from 'react';
import axios from '../../axiosBase';

import './AddQuote.css';

import Navigation from "../../components/Navigation/Navigation";
import Button from "../../components/UI/Button/Button";
import {CATEGORIES} from "../../constants";
import Spinner from "../../components/UI/Spinner/Spinner";
import withLoader from '../../hoc/withLoader';

class AddQuote extends Component {

  state = {
    author: '',
    quote: '',
    category: Object.keys(CATEGORIES)[0],
    loading: false,
  };

  changeQuote = event => {
      this.setState({quote: event.target.value})
  };
  changeAuthor = event => {
    this.setState({author: event.target.value})
  };
  changeCategory = event => {
    this.setState({category: event.target.value})
  };

  addQuote = event => {
    event.preventDefault();

    const quote = {
      category: this.state.category,
      author: this.state.author,
      quote: this.state.quote
    };

    axios.post('quotes.json', quote).then(() => {
      this.props.history.push('/')
    });
  };

  render() {
    let form = (
      <div className="form">
      <form className='formInput'>
        <label htmlFor="category">Category:</label>
        <select name="category" id="category"  onChange={this.changeCategory}>
          <option disabled defaultValue>-- select an option --</option>
          {Object.keys(CATEGORIES).map(categoryId => (
            <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
          ))}
        </select>
        <label htmlFor="author">Author:</label>
        <input id='author' type="text" onChange={this.changeAuthor}/>
        <label htmlFor="quote">Quote text:</label>
        <textarea id='qoute' type="text" onChange={this.changeQuote}/>
      </form>
      <Button
        onClick={this.addQuote}
      >
        Add quote
      </Button>
    </div>);

    if (this.state.loading) {
      form = <Spinner/>
    }

    return (
      <div>
        <Navigation />
        {form}
      </div>
    );
  }
}

export default withLoader(AddQuote, axios);