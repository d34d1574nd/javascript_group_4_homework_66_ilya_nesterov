import React, {Component} from 'react';
import NavLink from "react-router-dom/es/NavLink";
import axios from '../../axiosBase';
import {CATEGORIES} from "../../constants";

import './QuotesPage.css';

import Navigation from "../../components/Navigation/Navigation";
import Button from "../../components/UI/Button/Button";
import withLoader from '../../hoc/withLoader';

class QuotesPage extends Component {
  state = {
      quote: null,
  };

  loadData() {
    let url = 'quotes.json';

    if (this.props.match.params.categoryId) {
      url += `?orderBy="category"&equalTo="${this.props.match.params.categoryId}"`
    }

    axios.get(url).then(response => {
      if (response.data === null) {
        const quote = [];
        this.setState({quote});
        if(!response.data) return
      }

      const quote = Object.keys(response.data).map(id => {
        return {...response.data[id], id}
      });

      this.setState({quote: quote});
    })
  }

  componentDidMount() {
    this.loadData();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
      this.loadData()
    }
  }

  editQuote = index => {
    this.props.history.push("quotes/" + index + "/edit");
  };

  deleteQuote = index => {
    axios.delete("quotes/" + index + ".json").then(() => {
      this.props.history.push('/');
      this.loadData();
    })
  };

  render() {
    let quote = null;


    if (this.state.quote) {
      quote = this.state.quote.map((quote, index) => (
        <div className="quote" key={index}>
          <p className='text'>"{quote.quote}"</p>
          <p className='author'>  &mdash;  {quote.author}</p>
          <Button
            btnType='quoteButton edit'
            onClick={() => this.editQuote(quote.id)}
          ><i className="far fa-edit"></i></Button>
          <Button
            btnType='quoteButton delete'
            onClick={() => this.deleteQuote(quote.id)}
          ><i className="fas fa-times"></i></Button>
        </div>
      ))
    }
    return (
      <div>
        <Navigation />
        <div className="category">
          <ul className='qoutecategory'>
          <li><NavLink to="/" exact>All Quotes</NavLink></li>
            {Object.keys(CATEGORIES).map(categoryId => (
              <li
                key={categoryId}
              >
                <NavLink
                  to={categoryId}
                  exact
                >
                  {CATEGORIES[categoryId]}
                </NavLink>
              </li>
            ))}
          </ul>
        </div>
        <div className="quotes">
          {quote}
        </div>
      </div>
    );
  }
}

export default withLoader(QuotesPage, axios);