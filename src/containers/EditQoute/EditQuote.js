import React, {Component} from 'react';

import './EditQuote.css';

import Navigation from "../../components/Navigation/Navigation";
import axios from "../../axiosBase";
import {CATEGORIES} from "../../constants";
import Button from "../../components/UI/Button/Button";

class EditQuote extends Component {

  state = {
    author: '',
    quote: '',
    category:  '',
  };

  componentDidMount() {
    axios.get('quotes/' + this.props.match.params.id + '.json').then(response => {
      this.setState({author: response.data.author, quote: response.data.quote, category: response.data.category})
    })
  }

  changeQuote = event => {
    this.setState({quote: event.target.value})
  };
  changeAuthor = event => {
    this.setState({author: event.target.value})
  };
  changeCategory = event => {
    this.setState({category: event.target.value})
  };

  editQuote = event => {
    event.preventDefault();

    const quote = {
      category: this.state.category,
      author: this.state.author,
      quote: this.state.quote
    };

    axios.put('quotes/' + this.props.match.params.id + '.json', quote).then(() => {
      this.props.history.push('/')
    });
  };

  render() {
    console.log(this.state.category);
    return (
      <div>
        <Navigation />
        <div className="form">
          <form className='formInput'>
            <label htmlFor="category">Category:</label>
            <select name="category" id="category"  onChange={this.changeCategory} value={this.state.category}>
              {Object.keys(CATEGORIES).map(categoryId => (
                <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
              ))}
            </select>
            <label htmlFor="author">Author:</label>
            <input id='author' type="text" onChange={this.changeAuthor} value={this.state.author}/>
            <label htmlFor="quote">Quote text:</label>
            <textarea id='qoute' type="text" onChange={this.changeQuote} value={this.state.quote}/>
          </form>
          <Button
            onClick={this.editQuote}
          >
            Edit quote
          </Button>
        </div>
      </div>
    );
  }
}

export default EditQuote;