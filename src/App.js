import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import './App.css';

import QuotesPage from './containers/QuotesPage/QuotesPage';
import AddQuote from "./containers/AddQoute/AddQuote";
import EditQuote from "./containers/EditQoute/EditQuote";

class App extends Component {

    render() {
        return (
          <BrowserRouter>
            <div className="container">
              <Switch>
                <Route path="/" exact component={QuotesPage} />
                <Route path="/add" component={AddQuote} />
                <Route path="/quotes/:id/edit" component={EditQuote} />
                <Route path="/:categoryId" component={QuotesPage} />
                <Route render={() => <h1>Not found</h1>} />
              </Switch>
            </div>
          </BrowserRouter>
        );
    }
}

export default App;
