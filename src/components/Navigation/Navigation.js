import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

import './Navigation.css';

class Navigation extends Component {
  render() {
    return (
      <header className='header'>
        <NavLink to='/' className='navlink'>Quotes Page</NavLink>
        <NavLink to='/' className='navlink'>All Quotes</NavLink>
        <NavLink to='/add' className='navlink'>Submit new quote</NavLink>
      </header>
    );
  }
}

export default Navigation;