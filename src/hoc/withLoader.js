import React, {Component, Fragment} from 'react';

import Spinner from "../components/UI/Spinner/Spinner";

const withLoader = (SubComponent, axios) => {
    return class loaderHOC extends Component {
        constructor(props) {
            super (props);
            this.state = {
                loading: false,
                error: null,
            };

            this.state.interceptorReq = axios.interceptors.request.use(req => {
                this.setState({loading: true});
                return req
            }, error =>  {
                console.log(error);
                this.setState({error});
                throw error;
            });

            this.state.interceptorRes = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res
            }, error =>  {
                console.log(error);
                this.setState({error});
                throw error;
            });
        };

        componentWillUnmount() {
            axios.interceptors.request.eject(this.state.interceptorRes);
            axios.interceptors.response.eject(this.state.interceptorReq);
        }

        render () {
            return (
                <Fragment>
                    <SubComponent {...this.props}/>
                    {this.state.loading ? <Spinner/> : null}
                </Fragment>
            )
        }
    }
};


export default withLoader;
